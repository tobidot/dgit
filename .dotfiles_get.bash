#!/bin/bash
git clone --bare https://gitlab.com/tobidot/dgit.git "$HOME/.dotfiles_git"

function dgit {
   git --git-dir="$HOME/.dotfiles_git/" --work-tree="$HOME" "$@"
}

echo "Backing up pre-existing dot files.";
mkdir -p .dotfiles_backup
dgit checkout 2>&1 | grep -E "\s+\." | awk {'print $1'} | xargs -I{} mv {} .dotfiles_backup/{}

dgit checkout
dgit config status.showUntrackedFiles no
